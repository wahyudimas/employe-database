//
//  main.m
//  EmployeeDatabase
//
//  Created by Wahyudimas Hutama Rachman Syarief on 5/22/17.
//  Copyright © 2017 Wahyudimas Hutama Rachman Syarief. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
